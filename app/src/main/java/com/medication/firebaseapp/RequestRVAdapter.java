package com.medication.firebaseapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



import java.util.ArrayList;

public class RequestRVAdapter extends RecyclerView.Adapter<RequestRVAdapter.ViewHolder>{
    private ArrayList<RequestRVModal> reqRVModalArrayList;
    private Context context;
    private RequestRVAdapter.ReqClickInterface reqClickInterface;
    int lastPos = -1;

    //creating a constructor.
    public RequestRVAdapter(ArrayList<RequestRVModal> reqRVModalArrayList, Context context, RequestRVAdapter.ReqClickInterface reqClickInterface) {
        this.reqRVModalArrayList = reqRVModalArrayList;
        this.context = context;
        this.reqClickInterface = reqClickInterface;
    }

    @NonNull
    @Override
    public RequestRVAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //inflating our layout file on below line.
        View view = LayoutInflater.from(context).inflate(R.layout.request_rv_item, parent, false);
        return new RequestRVAdapter.ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull RequestRVAdapter.ViewHolder holder, int position) {
        //setting data to our recycler view item on below line.
        RequestRVModal requestRVModal = reqRVModalArrayList.get(position);
        holder.USERTV.setText(requestRVModal.getName());
        holder.MEDTV.setText(requestRVModal.getMedName());

        setAnimation(holder.itemView, position);
        holder.USERTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reqClickInterface.onReqClick(position);
            }
        });
        holder.MEDTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reqClickInterface.onReqClick(position);
            }
        });
    }

    private void setAnimation(View itemView, int position) {
        if (position > lastPos) {
            //on below line we are setting animation.
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            itemView.setAnimation(animation);
            lastPos = position;
        }
    }

    @Override
    public int getItemCount() {
        return reqRVModalArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //creating variable for our image view and text view on below line.

        private TextView USERTV;
        private TextView MEDTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //initializing all our variables on below line.
            USERTV  = itemView.findViewById(R.id.idTVUserName);
            MEDTV  = itemView.findViewById(R.id.idTVMissedMed);

        }
    }

    //creating a interface for on click
    public interface ReqClickInterface {
        void onReqClick(int position);
    }
}
