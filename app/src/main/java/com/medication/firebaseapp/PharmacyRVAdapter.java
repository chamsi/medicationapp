package com.medication.firebaseapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PharmacyRVAdapter extends RecyclerView.Adapter<PharmacyRVAdapter.ViewHolder>{
    private ArrayList<PharmacyRVModal> pharmacyRVModalArrayList;
    private Context context;
    private PharmacyRVAdapter.PhaClickInterface phaClickInterface;
    int lastPos = -1;

    //creating a constructor.
    public PharmacyRVAdapter(ArrayList<PharmacyRVModal> pharmacyRVModalArrayList, Context context, PharmacyRVAdapter.PhaClickInterface phaClickInterface) {
        this.pharmacyRVModalArrayList = pharmacyRVModalArrayList;
        this.context = context;
        this.phaClickInterface = phaClickInterface;
    }

    @NonNull
    @Override
    public PharmacyRVAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //inflating our layout file on below line.
        View view = LayoutInflater.from(context).inflate(R.layout.pharmacy_rv_item, parent, false);
        return new PharmacyRVAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PharmacyRVAdapter.ViewHolder holder, int position) {
        //setting data to our recycler view item on below line.
        PharmacyRVModal pharmacyRVModal = pharmacyRVModalArrayList.get(position);
        holder.phaTV.setText(pharmacyRVModal.getPhaName());
        Picasso.get().load(pharmacyRVModal.getPhaImg()).into(holder.phaIV);
        //adding animation to recycler view item on below line.
        setAnimation(holder.itemView, position);
        holder.phaIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phaClickInterface.onPhaClick(position);
            }
        });
    }

    private void setAnimation(View itemView, int position) {
        if (position > lastPos) {
            //on below line we are setting animation.
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            itemView.setAnimation(animation);
            lastPos = position;
        }
    }

    @Override
    public int getItemCount() {
        return pharmacyRVModalArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //creating variable for our image view and text view on below line.
        private ImageView phaIV;
        private TextView phaTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //initializing all our variables on below line.
            phaIV = itemView.findViewById(R.id.idIVPharmacy);
            phaTV = itemView.findViewById(R.id.idTVPharmacyName);

        }
    }

    //creating a interface for on click
    public interface PhaClickInterface {
        void onPhaClick(int position);
    }
}
