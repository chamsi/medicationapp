package com.medication.firebaseapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainActivity2 extends AppCompatActivity implements PharmacyRVAdapter.PhaClickInterface {

    private FloatingActionButton addPharmacyFAB,GoToMed,GoToReq;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private RecyclerView pharmacyRV;
    private FirebaseAuth mAuth;
    private ProgressBar loadingPB;
    private ArrayList<PharmacyRVModal> pharmacyRVModalArrayList;
    private PharmacyRVAdapter pharmacyRVAdapter;
    private RelativeLayout homeRL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        pharmacyRV = findViewById(R.id.idRVPharmacies);
        homeRL = findViewById(R.id.idRLBSheet1);
        loadingPB = findViewById(R.id.idPBLoading);
        addPharmacyFAB = findViewById(R.id.idFABAddPharmacy);
        GoToMed= findViewById(R.id.idGoMed);
        GoToReq = findViewById(R.id.idGoRequest);
        firebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        pharmacyRVModalArrayList = new ArrayList<>();
        //on below line we are getting database reference.
        databaseReference = firebaseDatabase.getReference("Pharmacies");
        //on below line adding a click listener for our floating action button.
        addPharmacyFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opening a new activity for adding a course.
                Intent i = new Intent(MainActivity2.this, AddPharmacyActivity.class);
                startActivity(i);
                finish();
            }
        });
        GoToReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opening a new activity for adding a course.
                Intent i = new Intent(MainActivity2.this, Requests.class);
                startActivity(i);
                finish();
            }
        });
        GoToMed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opening a new activity for adding a course.
                Intent i = new Intent(MainActivity2.this, MainActivity.class);
                startActivity(i);
                finish();

            }
        });
        pharmacyRVAdapter = new PharmacyRVAdapter(pharmacyRVModalArrayList, this, this::onPhaClick);
        //setting layout malinger to recycler view on below line.
        pharmacyRV.setLayoutManager(new LinearLayoutManager(this));
        //setting adapter to recycler view on below line.
        pharmacyRV.setAdapter(pharmacyRVAdapter);
        //on below line calling a method to fetch courses from database.
        getPharmacies();
    }
    private void getPharmacies() {
        //on below line clearing our list.
        pharmacyRVModalArrayList.clear();
        //on below line we are calling add child event listener method to read the data.
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //on below line we are hiding our progress bar.
                loadingPB.setVisibility(View.GONE);
                //adding snapshot to our array list on below line.
                pharmacyRVModalArrayList.add(snapshot.getValue(PharmacyRVModal.class));
                //notifying our adapter that data has changed.
                pharmacyRVAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //this method is called when new child is added we are notifying our adapter and making progress bar visibility as gone.
                loadingPB.setVisibility(View.GONE);
                pharmacyRVAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                //notifying our adapter when child is removed.
                pharmacyRVAdapter.notifyDataSetChanged();
                loadingPB.setVisibility(View.GONE);

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //notifying our adapter when child is moved.
                pharmacyRVAdapter.notifyDataSetChanged();
                loadingPB.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    @Override
    public void onPhaClick(int position) {
        displayBottomSheet(pharmacyRVModalArrayList.get(position));
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //adding a click listner for option selected on below line.
        int id = item.getItemId();
        switch (id) {
            case R.id.idLogOut:
                //displaying a toast message on user logged out inside on click.
                Toast.makeText(getApplicationContext(), "User Logged Out", Toast.LENGTH_LONG).show();
                //on below line we are signing out our user.
                mAuth.signOut();
                //on below line we are opening our login activity.
                Intent i = new Intent(MainActivity2.this, LoginActivity.class);
                startActivity(i);
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //on below line we are inflating our menu file for displaying our menu options.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void displayBottomSheet(PharmacyRVModal modal) {
        //on below line we are creating our bottom sheet dialog.
        final BottomSheetDialog bottomSheetTeachersDialog = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);
        //on below line we are inflating our layout file for our bottom sheet.
        View layout = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_layout1, homeRL);
        //setting content view for bottom sheet on below line.
        bottomSheetTeachersDialog.setContentView(layout);
        //on below line we are setting a cancelable
        bottomSheetTeachersDialog.setCancelable(false);
        bottomSheetTeachersDialog.setCanceledOnTouchOutside(true);
        //calling a method to display our bottom sheet.
        bottomSheetTeachersDialog.show();
        //on below line we are creating variables for our text view and image view inside bottom sheet
        //and initialing them with their ids.
        TextView pharmacyNameTV = layout.findViewById(R.id.idTVPharmacyName);
        TextView pharmacyDescTV = layout.findViewById(R.id.idTVPharmacyDesc);
        TextView pharmacylocalTV = layout.findViewById(R.id.idTVLocal);

        ImageView pharmacyIV = layout.findViewById(R.id.idIVPharmacy);
        //on below line we are setting data to different views on below line.
        pharmacyNameTV.setText(modal.getPhaName());
        pharmacyDescTV.setText(modal.getPhaDescription());

        pharmacylocalTV.setText(modal.getPhaAddress());
        Picasso.get().load(modal.getPhaImg()).into(pharmacyIV);
        Button viewBtn = layout.findViewById(R.id.idBtnViewMap);
        Button editBtn = layout.findViewById(R.id.idBtnEditPha);

        //adding on click listener for our edit button.
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //on below line we are opening our EditCourseActivity on below line.
                Intent i = new Intent(MainActivity2.this, EditPharmacyActivity.class);
                //on below line we are passing our course modal
                i.putExtra("pharmacy", modal);
                startActivity(i);
            }
        });

        //adding click listener for our view button on below line.

        viewBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {


                String b=modal.getPhaAddress().toString();
                if ((b!="")&&(b!=null)) {

                    StringBuilder sb = new StringBuilder();
                    sb.append("geo:0,0?q=");
                    String addressEncoded = Uri.encode(b);
                    sb.append(addressEncoded);

                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sb.toString()));
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                    finish();

                }
            }});

    }


}