package com.medication.firebaseapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddMedActivity extends AppCompatActivity {

    //creating variables for our button, edit text,firebase database, database refrence, progress bar.
    private Button addMedBtn;
    private TextInputEditText medNameEdt, medDescEdt, medPriceEdt, bestSuitedEdt, medImgEdt, medLinkEdt;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private ProgressBar loadingPB;
    private String medID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_med);
        //initializing all our variables.
        addMedBtn = findViewById(R.id.idBtnAddCourse);
        medNameEdt = findViewById(R.id.idEdtCourseName);
        medDescEdt = findViewById(R.id.idEdtCourseDescription);
        medPriceEdt = findViewById(R.id.idEdtCoursePrice);
        bestSuitedEdt = findViewById(R.id.idEdtSuitedFor);
        medImgEdt = findViewById(R.id.idEdtCourseImageLink);
        medLinkEdt = findViewById(R.id.idEdtCourseLink);
        loadingPB = findViewById(R.id.idPBLoading);
        firebaseDatabase = FirebaseDatabase.getInstance();
        //on below line creating our database reference.
        databaseReference = firebaseDatabase.getReference("Meds");
        //adding click listener for our add course button.
        addMedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingPB.setVisibility(View.VISIBLE);
                //getting data from our edit text.
                String medName = medNameEdt.getText().toString();
                String medDesc = medDescEdt.getText().toString();
                String medPrice = medPriceEdt.getText().toString();
                String bestSuited = bestSuitedEdt.getText().toString();
                String medImg = medImgEdt.getText().toString();
                String medLink = medLinkEdt.getText().toString();
                medID = medName;
                //on below line we are passing all data to our modal class.
                MedRVModal medRVModal = new MedRVModal(medName, medDesc, medPrice, bestSuited, medImg, medLink,medID);
                //on below line we are calling a add value event to pass data to firebase database.
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        //on below line we are setting data in our firebase database.
                        databaseReference.child(medID).setValue(medRVModal);
                        //displaying a toast message.
                        Toast.makeText(AddMedActivity.this, "Med Added..", Toast.LENGTH_SHORT).show();
                        //starting a main activity.
                        startActivity(new Intent(AddMedActivity.this, MainActivity.class));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        //displaying a failure message on below line.
                        Toast.makeText(AddMedActivity.this, "Fail to add Med..", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}