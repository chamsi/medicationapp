package com.medication.firebaseapp;

import android.os.Parcel;
import android.os.Parcelable;

public class RequestRVModal implements Parcelable {
    private String Name ;
    private String Address ;
    private String MedName ;
    private String ReqId;


    public RequestRVModal(String name, String address, String MedName, String reqId) {
        Name = name;
        Address = address;
        this.MedName = MedName;
        ReqId = reqId;
    }
    public RequestRVModal() {

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getMedName() {
        return MedName;
    }

    public void setMedName(String MedName) {
        this.MedName = MedName;
    }

    public String getReqId() {
        return ReqId;
    }

    public void setReqId(String reqId) {
        ReqId = reqId;
    }
    protected RequestRVModal(Parcel in) {
        Name = in.readString();
        Address = in.readString();
        MedName = in.readString();
        ReqId = in.readString();
    }

    public static final Creator<RequestRVModal> CREATOR = new Creator<RequestRVModal>() {
        @Override
        public RequestRVModal createFromParcel(Parcel in) {
            return new RequestRVModal(in);
        }

        @Override
        public RequestRVModal[] newArray(int size) {
            return new RequestRVModal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(Name);
        parcel.writeString(Address);
        parcel.writeString(MedName);
        parcel.writeString(ReqId);
    }

}
