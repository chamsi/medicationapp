package com.medication.firebaseapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;

import java.util.ArrayList;

public class Requests extends AppCompatActivity {

    private FloatingActionButton GoToPha,GoToMed;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private RecyclerView requestRV;
    private FirebaseAuth mAuth;
    private ProgressBar loadingPB;
    private ArrayList<RequestRVModal> requestRVModalArrayList;
    private RequestRVAdapter requestRVAdapter;
    private RelativeLayout homeRL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);
        requestRV = findViewById(R.id.idRVRequests);
        homeRL = findViewById(R.id.idRLBSheet2);
        loadingPB = findViewById(R.id.idPBLoading);

        GoToMed= findViewById(R.id.idGoMed);
        GoToPha= findViewById(R.id.idGoPharmacy);
        firebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        requestRVModalArrayList = new ArrayList<>();
        //on below line we are getting database reference.
        databaseReference = firebaseDatabase.getReference("suggestions");
        //on below line adding a click listener for our floating action button.
        GoToPha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opening a new activity for adding a course.
                Intent i = new Intent(Requests.this, MainActivity2.class);
                startActivity(i);
            }
        });
        GoToMed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opening a new activity for adding a course.
                Intent i = new Intent(Requests.this, MainActivity.class);
                startActivity(i);

            }
        });
        requestRVAdapter = new RequestRVAdapter(requestRVModalArrayList, this, this::onReqClick);
        //setting layout malinger to recycler view on below line.
        requestRV.setLayoutManager(new LinearLayoutManager(this));
        //setting adapter to recycler view on below line.
        requestRV.setAdapter(requestRVAdapter);
        //on below line calling a method to fetch courses from database.
        getRequests();
    }
    private void getRequests() {
        //on below line clearing our list.
        requestRVModalArrayList.clear();
        //on below line we are calling add child event listener method to read the data.
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //on below line we are hiding our progress bar.
                loadingPB.setVisibility(View.GONE);
                //adding snapshot to our array list on below line.
                requestRVModalArrayList.add(snapshot.getValue(RequestRVModal.class));
                //notifying our adapter that data has changed.
                requestRVAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //this method is called when new child is added we are notifying our adapter and making progress bar visibility as gone.
                loadingPB.setVisibility(View.GONE);
                requestRVAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                //notifying our adapter when child is removed.
                requestRVAdapter.notifyDataSetChanged();
                loadingPB.setVisibility(View.GONE);

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                //notifying our adapter when child is moved.
                requestRVAdapter.notifyDataSetChanged();
                loadingPB.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void onReqClick(int position) {
        displayBottomSheet(requestRVModalArrayList.get(position));
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //adding a click listner for option selected on below line.
        int id = item.getItemId();
        switch (id) {
            case R.id.idLogOut:
                //displaying a toast message on user logged out inside on click.
                Toast.makeText(getApplicationContext(), "User Logged Out", Toast.LENGTH_LONG).show();
                //on below line we are signing out our user.
                mAuth.signOut();
                //on below line we are opening our login activity.
                Intent i = new Intent(Requests.this, LoginActivity.class);
                startActivity(i);
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //on below line we are inflating our menu file for displaying our menu options.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void displayBottomSheet(RequestRVModal modal) {
        //on below line we are creating our bottom sheet dialog.
        final BottomSheetDialog bottomSheetTeachersDialog = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);
        //on below line we are inflating our layout file for our bottom sheet.
        View layout = LayoutInflater.from(this).inflate(R.layout.bottom_sheets_requests, homeRL);
        //setting content view for bottom sheet on below line.
        bottomSheetTeachersDialog.setContentView(layout);
        //on below line we are setting a cancelable
        bottomSheetTeachersDialog.setCancelable(false);
        bottomSheetTeachersDialog.setCanceledOnTouchOutside(true);
        //calling a method to display our bottom sheet.
        bottomSheetTeachersDialog.show();
        //on below line we are creating variables for our text view and image view inside bottom sheet
        //and initialing them with their ids.

        TextView requestNameTV = layout.findViewById(R.id.idTVUserName);
        TextView requestAddressTV = layout.findViewById(R.id.idTVUserAddress);
        TextView requestMissedTV = layout.findViewById(R.id.idTVMissedMed);

        requestNameTV.setText(modal.getName());
        requestAddressTV.setText(modal.getAddress());

        requestMissedTV.setText(modal.getMedName());

        Button DeleteBtn = layout.findViewById(R.id.idBtnDeleteRequest);
        Button sendBtn = layout.findViewById(R.id.idBtnSendNotification);

        //adding on click listener for our edit button.
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //on below line we are opening our EditCourseActivity on below line.
                Intent i = new Intent(Requests.this, SendActivity3.class);
                //on below line we are passing our course modal

                startActivity(i);
            }
        });

        //adding click listener for our view button on below line.

        DeleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                databaseReference.removeValue();

                startActivity(new Intent(Requests.this, Requests.class));
            }});

    }


}

