package com.medication.firebaseapp;

import android.os.Parcel;
import android.os.Parcelable;

public class MedRVModal implements Parcelable {
    //creating variables for our different fields.
    private String medName;
    private String medDescription;
    private String medPrice;
    private String bestSuitedFor;
    private String medImg;
    private String medLink;
    private String medId;


    public String getMedId() {
        return medId;
    }

    public void setMedId(String medId) {
        this.medId = medId;
    }


    //creating an empty constructor.
    public MedRVModal() {

    }

    protected MedRVModal(Parcel in) {
        medName = in.readString();
        medId = in.readString();
        medDescription = in.readString();
        medPrice = in.readString();
        bestSuitedFor = in.readString();
        medImg = in.readString();
        medLink = in.readString();
    }

    public static final Creator<MedRVModal> CREATOR = new Creator<MedRVModal>() {
        @Override
        public MedRVModal createFromParcel(Parcel in) {
            return new MedRVModal(in);
        }

        @Override
        public MedRVModal[] newArray(int size) {
            return new MedRVModal[size];
        }
    };

    public String getMedName() {
        return medName;
    }

    public void setMedName(String medName) {
        this.medName = medName;
    }

    public String getMedDescription() {
        return medDescription;
    }

    public void setMedDescription(String medDescription) {
        this.medDescription = medDescription;
    }

    public String getMedPrice() {
        return medPrice;
    }

    public void setMedPrice(String medPrice) {
        this.medPrice = medPrice;
    }

    public String getBestSuitedFor() {
        return bestSuitedFor;
    }

    public void setBestSuitedFor(String bestSuitedFor) {
        this.bestSuitedFor = bestSuitedFor;
    }

    public String getMedImg() {
        return medImg;
    }

    public void setMedImg(String medImg) {
        this.medImg = medImg;
    }

    public String getMedLink() {
        return medLink;
    }

    public void setMedLink(String medLink) {
        this.medLink = medLink;
    }

    public MedRVModal(String medName, String medDescription, String medPrice, String bestSuitedFor, String medImg, String medLink, String medId) {
        this.medName = medName;
        this.medDescription = medDescription;
        this.medPrice = medPrice;
        this.bestSuitedFor = bestSuitedFor;
        this.medImg = medImg;
        this.medLink = medLink;
        this.medId = medId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(medName);
        dest.writeString(medId);
        dest.writeString(medDescription);
        dest.writeString(medPrice);
        dest.writeString(bestSuitedFor);
        dest.writeString(medImg);
        dest.writeString(medLink);
    }
}
