package com.medication.firebaseapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

public class SendActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send3);
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        EditText title = findViewById(R.id.editTextNotificationName);
        EditText body = findViewById(R.id.editTextNotificationBody);


        findViewById(R.id.idbutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!title.toString().isEmpty() && !body.toString().isEmpty()){
                    FcmNotificationsSender notificationsSender = new FcmNotificationsSender("dbWkEj0gT26_rqcCjuonHl:APA91bHUVZJW0wfmUQSJaQyui4_olIOy_c1pKo5pchZOoB1i7tFRVWmyJlLMnPsGWMwo1dvy_Y1n8UPymw9ZysfaB3qNsH70rt260ZUsChq0FsqRnQRff82Jftr1pOfRippUuBtuyUHV",title.getText().toString(),body.getText().toString(),getApplicationContext(),SendActivity3.this);
                    notificationsSender.SendNotifications();
                    Toast.makeText(SendActivity3.this, "Notification is Sent", Toast.LENGTH_SHORT).show();
                    //starting a main activity.
                    startActivity(new Intent(SendActivity3.this, Requests.class));
                    finish();
                }else {
                    Toast.makeText(SendActivity3.this,"write some text",Toast.LENGTH_LONG).show();
                }
            }
        });



    }

}