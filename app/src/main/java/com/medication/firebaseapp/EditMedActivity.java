package com.medication.firebaseapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class EditMedActivity extends AppCompatActivity {

    //creating variables for our edit text, firebase database, database reference, course rv modal,progress bar.
    private TextInputEditText medNameEdt, medDescEdt, medPriceEdt, bestSuitedEdt, medImgEdt, medLinkEdt;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    MedRVModal medRVModal;
    private ProgressBar loadingPB;
    //creating a string for our course id.
    private String medID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_med);
        //initializing all our variables on below line.
        Button addMedBtn = findViewById(R.id.idBtnUpdateCourse);
        medNameEdt = findViewById(R.id.idEdtCourseName);
        medDescEdt = findViewById(R.id.idEdtCourseDescription);
        medPriceEdt = findViewById(R.id.idEdtCoursePrice);
        bestSuitedEdt = findViewById(R.id.idEdtSuitedFor);
        medImgEdt = findViewById(R.id.idEdtCourseImageLink);
        medLinkEdt = findViewById(R.id.idEdtCourseLink);
        loadingPB = findViewById(R.id.idPBLoading);
        firebaseDatabase = FirebaseDatabase.getInstance();
        //on below line we are getting our modal class on which we have passed.
        medRVModal = getIntent().getParcelableExtra("med");
        Button deleteMedBtn = findViewById(R.id.idBtnDeleteCourse);

        if (medRVModal != null) {
            //on below line we are setting data to our edit text from our modal class.
            medNameEdt.setText(medRVModal.getMedName());
            medPriceEdt.setText(medRVModal.getMedPrice());
            bestSuitedEdt.setText(medRVModal.getBestSuitedFor());
            medImgEdt.setText(medRVModal.getMedImg());
            medLinkEdt.setText(medRVModal.getMedLink());
            medDescEdt.setText(medRVModal.getMedDescription());
            medID = medRVModal.getMedId();
        }

        //on below line we are initialing our database reference and we are adding a child as our course id.
        databaseReference = firebaseDatabase.getReference("Meds").child(medID);


        //on below line we are adding click listener for our add course button.
        addMedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //on below line we are making our progress bar as visible.
                loadingPB.setVisibility(View.VISIBLE);
                //on below line we are getting data from our edit text.
                String medName = medNameEdt.getText().toString();
                String medDesc = medDescEdt.getText().toString();
                String medPrice = medPriceEdt.getText().toString();
                String bestSuited = bestSuitedEdt.getText().toString();
                String medImg = medImgEdt.getText().toString();
                String medLink = medLinkEdt.getText().toString();
                //on below line we are creating a map for passing a data using key and value pair.
                Map<String, Object> map = new HashMap<>();
                map.put("medName", medName);
                map.put("medDescription", medDesc);
                map.put("medPrice", medPrice);
                map.put("bestSuitedFor", bestSuited);
                map.put("medImg", medImg);
                map.put("medLink", medLink);
                map.put("medId", medID);

                //on below line we are calling a database reference on add value event listener and on data change method
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        //making progress bar visibility as gone.
                        loadingPB.setVisibility(View.GONE);
                        //adding a map to our database.
                        databaseReference.updateChildren(map);
                        //on below line we are displaying a toast message.
                        Toast.makeText(EditMedActivity.this, "Med Updated..", Toast.LENGTH_SHORT).show();
                        //opening a new activity after updating our coarse.
                        startActivity(new Intent(EditMedActivity.this, MainActivity.class));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        //displaying a failure message on toast.
                        Toast.makeText(EditMedActivity.this, "Fail to update med..", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        //adding a click listener for our delete course button.
        deleteMedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //calling a method to delete a course.
                deleteMed();
            }
        });

    }

    private void deleteMed() {
        //on below line calling a method to delete the course.
        databaseReference.removeValue();
        //displaying a toast message on below line.
        Toast.makeText(this, "Med Deleted..", Toast.LENGTH_SHORT).show();
        //opening a main activity on below line.
        startActivity(new Intent(EditMedActivity.this, MainActivity.class));
    }
}