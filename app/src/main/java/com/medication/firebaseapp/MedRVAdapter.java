package com.medication.firebaseapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MedRVAdapter extends RecyclerView.Adapter<MedRVAdapter.ViewHolder> {
    //creating variables for our list, context, interface and position.
    private ArrayList<MedRVModal> medRVModalArrayList;
    private Context context;
    private MedClickInterface medClickInterface;
    int lastPos = -1;

    //creating a constructor.
    public MedRVAdapter(ArrayList<MedRVModal> medRVModalArrayList, Context context, MedClickInterface medClickInterface) {
        this.medRVModalArrayList = medRVModalArrayList;
        this.context = context;
        this.medClickInterface = medClickInterface;
    }

    @NonNull
    @Override
    public MedRVAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //inflating our layout file on below line.
        View view = LayoutInflater.from(context).inflate(R.layout.med_rv_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MedRVAdapter.ViewHolder holder, int position) {
        //setting data to our recycler view item on below line.
        MedRVModal medRVModal = medRVModalArrayList.get(position);
        holder.medTV.setText(medRVModal.getMedName());
        holder.medPriceTV.setText("$. " + medRVModal.getMedPrice());
        Picasso.get().load(medRVModal.getMedImg()).into(holder.medIV);
        //adding animation to recycler view item on below line.
        setAnimation(holder.itemView, position);
        holder.medIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medClickInterface.onMedClick(position);
            }
        });
    }

    private void setAnimation(View itemView, int position) {
        if (position > lastPos) {
            //on below line we are setting animation.
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            itemView.setAnimation(animation);
            lastPos = position;
        }
    }

    @Override
    public int getItemCount() {
        return medRVModalArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //creating variable for our image view and text view on below line.
        private ImageView medIV;
        private TextView medTV, medPriceTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //initializing all our variables on below line.
            medIV = itemView.findViewById(R.id.idIVCourse);
            medTV = itemView.findViewById(R.id.idTVCOurseName);
            medPriceTV = itemView.findViewById(R.id.idTVCousePrice);
        }
    }

    //creating a interface for on click
    public interface MedClickInterface {
        void onMedClick(int position);
    }
}
