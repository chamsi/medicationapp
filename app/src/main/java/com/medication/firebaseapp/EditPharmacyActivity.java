package com.medication.firebaseapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class EditPharmacyActivity extends AppCompatActivity {
    private Button updatePharmacyBtn , deletePharmacyBtn;
    private TextInputEditText PharmacyNameEdt, PharmacyDescEdt, PharmacyImgEdt, PharmacyAddressEdt,PharmacyMed1NameEdt,PharmacyMed2NameEdt,PharmacyMed3NameEdt,PharmacyMed4NameEdt,PharmacyMed5NameEdt;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private ProgressBar loadingPB;
    private String pharmacyID;

    PharmacyRVModal pharmacyRVModal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pharmacy);

                //initializing all our variables on below line.
                updatePharmacyBtn = findViewById(R.id.idBtnUpdatePharmacy);
                PharmacyNameEdt = findViewById(R.id.idEdtPharmacyName);
                PharmacyDescEdt = findViewById(R.id.idEdtPharmacyDescription);
                PharmacyAddressEdt = findViewById(R.id.idEdtPharmacyLocal);

                PharmacyImgEdt = findViewById(R.id.idEdtPharmacyImageLink);
                PharmacyMed1NameEdt = findViewById(R.id.idEdtPharmacyMed1);
                PharmacyMed2NameEdt = findViewById(R.id.idEdtPharmacyMed2);
                PharmacyMed3NameEdt = findViewById(R.id.idEdtPharmacyMed3);
                PharmacyMed4NameEdt = findViewById(R.id.idEdtPharmacyMed4);
                PharmacyMed5NameEdt = findViewById(R.id.idEdtPharmacyMed5);
                loadingPB = findViewById(R.id.idPBLoading);
                firebaseDatabase = FirebaseDatabase.getInstance();
                //on below line we are getting our modal class on which we have passed.
                pharmacyRVModal = getIntent().getParcelableExtra("pharmacy");
                deletePharmacyBtn = findViewById(R.id.idBtnDeletePharmacy);

                if (pharmacyRVModal != null) {
                    //on below line we are setting data to our edit text from our modal class.
                    PharmacyNameEdt.setText(pharmacyRVModal.getPhaName());
                    PharmacyDescEdt.setText(pharmacyRVModal.getPhaDescription());
                    PharmacyAddressEdt.setText(pharmacyRVModal.getPhaAddress());
                    PharmacyMed1NameEdt.setText(pharmacyRVModal.getMed1Name());
                    PharmacyMed2NameEdt.setText(pharmacyRVModal.getMed2Name());
                    PharmacyMed3NameEdt.setText(pharmacyRVModal.getMed3Name());
                    PharmacyMed4NameEdt.setText(pharmacyRVModal.getMed4Name());
                    PharmacyMed5NameEdt.setText(pharmacyRVModal.getMed5Name());
                    PharmacyImgEdt.setText(pharmacyRVModal.getPhaImg());


                    pharmacyID = pharmacyRVModal.getPhaId();
                }

                //on below line we are initialing our database reference and we are adding a child as our course id.
                databaseReference = firebaseDatabase.getReference("Pharmacies").child(pharmacyID);


                //on below line we are adding click listener for our add course button.
        updatePharmacyBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //on below line we are making our progress bar as visible.
                        loadingPB.setVisibility(View.VISIBLE);
                        //on below line we are getting data from our edit text.
                        String PharmacyName = PharmacyNameEdt.getText().toString();
                        String PharmacyDesc = PharmacyDescEdt.getText().toString();
                        String PharmacyAddress = PharmacyAddressEdt.getText().toString();
                        String PharmacyMed1Name = PharmacyMed1NameEdt.getText().toString();
                        String PharmacyMed2Name = PharmacyMed2NameEdt.getText().toString();
                        String PharmacyMed3Name = PharmacyMed3NameEdt.getText().toString();
                        String PharmacyMed4Name = PharmacyMed4NameEdt.getText().toString();
                        String PharmacyMed5Name = PharmacyMed5NameEdt.getText().toString();
                        String PharmacyImg = PharmacyImgEdt.getText().toString();

                        //on below line we are creating a map for passing a data using key and value pair.
                        Map<String, Object> map = new HashMap<>();
                        map.put("PharmacyName", PharmacyName);
                        map.put("PharmacyDesc", PharmacyDesc);
                        map.put("PharmacyAddress", PharmacyAddress);
                        map.put("PharmacyMed1Name", PharmacyMed1Name);
                        map.put("PharmacyMed2Name", PharmacyMed2Name);
                        map.put("PharmacyMed3Name", PharmacyMed3Name);
                        map.put("PharmacyMed4Name", PharmacyMed4Name);
                        map.put("PharmacyMed5Name", PharmacyMed5Name);
                        map.put("PharmacyImg", PharmacyImg);
                        map.put("pharmacyID", pharmacyID);

                        //on below line we are calling a database reference on add value event listener and on data change method
                        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                //making progress bar visibility as gone.
                                loadingPB.setVisibility(View.GONE);
                                //adding a map to our database.
                                databaseReference.updateChildren(map);
                                //on below line we are displaying a toast message.
                                Toast.makeText(com.medication.firebaseapp.EditPharmacyActivity.this, "Pharmacy Updated..", Toast.LENGTH_SHORT).show();
                                //opening a new activity after updating our coarse.
                                startActivity(new Intent(com.medication.firebaseapp.EditPharmacyActivity.this, MainActivity2.class));
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                                //displaying a failure message on toast.
                                Toast.makeText(com.medication.firebaseapp.EditPharmacyActivity.this, "Fail to update pharmacy..", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

                //adding a click listener for our delete course button.
        deletePharmacyBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //calling a method to delete a course.
                        deletePharmacy();
                    }
                });

            }

            private void deletePharmacy() {
                //on below line calling a method to delete the course.
                databaseReference.removeValue();
                //displaying a toast message on below line.
                Toast.makeText(this, "Pharmacy Deleted..", Toast.LENGTH_SHORT).show();
                //opening a main activity on below line.
                startActivity(new Intent(com.medication.firebaseapp.EditPharmacyActivity.this, MainActivity2.class));
            }
        }

