package com.medication.firebaseapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddPharmacyActivity extends AppCompatActivity {


    private Button addPharmacyBtn;
    private TextInputEditText PharmacyNameEdt, PharmacyDescEdt, PharmacyImgEdt, PharmacyAddressEdt,PharmacyMed1NameEdt,PharmacyMed2NameEdt,PharmacyMed3NameEdt,PharmacyMed4NameEdt,PharmacyMed5NameEdt;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private ProgressBar loadingPB;
    private String pharmacyID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pharmacy);
        addPharmacyBtn = findViewById(R.id.idBtnAddPharmacy);
        PharmacyNameEdt = findViewById(R.id.idEdtPharmacyName);
        PharmacyDescEdt = findViewById(R.id.idEdtPharmacyDescription);
        PharmacyAddressEdt = findViewById(R.id.idEdtPharmacyLocal);

        PharmacyImgEdt = findViewById(R.id.idEdtPharmacyImageLink);
        PharmacyMed1NameEdt = findViewById(R.id.idEdtPharmacyMed1);
        PharmacyMed2NameEdt = findViewById(R.id.idEdtPharmacyMed2);
        PharmacyMed3NameEdt = findViewById(R.id.idEdtPharmacyMed3);
        PharmacyMed4NameEdt = findViewById(R.id.idEdtPharmacyMed4);
        PharmacyMed5NameEdt = findViewById(R.id.idEdtPharmacyMed5);

        loadingPB = findViewById(R.id.idPBLoading);
        firebaseDatabase = FirebaseDatabase.getInstance();
        //on below line creating our database reference.
        databaseReference = firebaseDatabase.getReference("Pharmacies");
        //adding click listener for our add course button.


        addPharmacyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingPB.setVisibility(View.VISIBLE);
                //getting data from our edit text.
                String phaName = PharmacyNameEdt.getText().toString();
                String phaDesc = PharmacyDescEdt.getText().toString();
                String phaAddress = PharmacyAddressEdt.getText().toString();
                String phaMed1Name = PharmacyMed1NameEdt.getText().toString();
                String phaMed2Name = PharmacyMed2NameEdt.getText().toString();
                String phaMed3Name = PharmacyMed3NameEdt.getText().toString();
                String phaMed4Name = PharmacyMed4NameEdt.getText().toString();
                String phaMed5Name = PharmacyMed5NameEdt.getText().toString();
                String phaImg = PharmacyImgEdt.getText().toString();

                pharmacyID = phaName;
                //on below line we are passing all data to our modal classString phaName, String phaAddress, String phaImg, String phaDescription, String Med1Name, String Med2Name, String Med3Name, String Med4Name, String Med5Name,String phaId
                PharmacyRVModal pharmacyRVModal = new PharmacyRVModal(phaName, phaAddress, phaImg, phaDesc, phaMed1Name, phaMed2Name,phaMed3Name,phaMed4Name,phaMed5Name,pharmacyID);
                //on below line we are calling a add value event to pass data to firebase database.
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        //on below line we are setting data in our firebase database.
                        databaseReference.child(pharmacyID).setValue(pharmacyRVModal);
                        //displaying a toast message.
                        Toast.makeText(AddPharmacyActivity.this, "Pharmacy Added..", Toast.LENGTH_SHORT).show();
                        //starting a main activity.
                        startActivity(new Intent(AddPharmacyActivity.this, MainActivity2.class));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        //displaying a failure message on below line.
                        Toast.makeText(AddPharmacyActivity.this, "Fail to add Pharmacy..", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}