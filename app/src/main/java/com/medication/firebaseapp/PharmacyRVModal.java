package com.medication.firebaseapp;

import android.os.Parcel;
import android.os.Parcelable;

public class PharmacyRVModal implements Parcelable {
    //creating variables for our different fields.
    private String phaName;
    private String phaAddress;
    private String phaImg;
    private String phaDescription;
    private String Med1Name;
    private String Med2Name;
    private String Med3Name;
    private String Med4Name;
    private String Med5Name;
    private String phaId;

    //creating an empty constructor.
    public PharmacyRVModal() {

    }
    protected PharmacyRVModal(Parcel in) {
        phaName = in.readString();
        phaAddress = in.readString();
        phaImg = in.readString();
        phaDescription = in.readString();
        Med1Name = in.readString();
        Med2Name = in.readString();
        Med3Name = in.readString();
        Med4Name = in.readString();
        Med5Name = in.readString();
        phaId = in.readString();
    }

    public String getPhaName() {
        return phaName;
    }

    public void setPhaName(String phaName) {
        this.phaName = phaName;
    }

    public String getPhaAddress() {
        return phaAddress;
    }

    public void setPhaAddress(String phaAddress) {
        this.phaAddress = phaAddress;
    }

    public String getPhaImg() {
        return phaImg;
    }

    public void setPhaImg(String phaImg) {
        this.phaImg = phaImg;
    }

    public String getPhaDescription() {
        return phaDescription;
    }

    public void setPhaDescription(String phaDescription) {
        this.phaDescription = phaDescription;
    }

    public String getMed1Name() {
        return Med1Name;
    }

    public void setMed1Name(String Med1Name) {
        this.Med1Name = Med1Name;
    }

    public String getMed2Name() {
        return Med2Name;
    }

    public void setMed2Name(String Med2Name) {
        this.Med2Name = Med2Name;
    }

    public String getMed3Name() {
        return Med3Name;
    }

    public void setMed3Name(String Med3Name) {
        this.Med3Name = Med3Name;
    }

    public String getMed4Name() {
        return Med4Name;
    }

    public void setMed4Name(String Med4Name) {
        this.Med4Name = Med4Name;
    }

    public String getMed5Name() {
        return Med5Name;
    }

    public void setMed5Name(String Med5Name) {
        this.Med5Name = Med5Name;
    }

    public String getPhaId() {
        return phaId;
    }

    public void setPhaId(String phaId) {
        this.phaId = phaId;
    }

    public static final Creator<PharmacyRVModal> CREATOR = new Creator<PharmacyRVModal>() {
        @Override
        public PharmacyRVModal createFromParcel(Parcel in) {
            return new PharmacyRVModal(in);
        }

        @Override
        public PharmacyRVModal[] newArray(int size) {
            return new PharmacyRVModal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
    public PharmacyRVModal(String phaName, String phaAddress, String phaImg, String phaDescription, String Med1Name, String Med2Name, String Med3Name, String Med4Name, String Med5Name,String phaId) {
        this.phaName = phaName;
        this.phaAddress = phaAddress;
        this.phaImg = phaImg;
        this.phaDescription = phaDescription;
        this.Med1Name = Med1Name;
        this.Med2Name = Med2Name;
        this.Med3Name = Med3Name;
        this.Med4Name = Med4Name;
        this.Med5Name = Med5Name;
        this.phaId=phaId;
    }
    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(phaName);
        parcel.writeString(phaAddress);
        parcel.writeString(phaImg);
        parcel.writeString(phaDescription);
        parcel.writeString(Med1Name);
        parcel.writeString(Med2Name);
        parcel.writeString(Med3Name);
        parcel.writeString(Med4Name);
        parcel.writeString(Med5Name);
        parcel.writeString(phaId);
    }


}
